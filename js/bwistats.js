var bwistats = {

    // keyed by quartile 1..4
    qdata: null,
    // data and categories keyed by quartile
    data: null,
    categories: null,

    qcurrent: 1,
    qcurrentIndex: 0,
    qmaxItems:10,
    qlastIndex:10,

    // Prepare dataSet for display - divide into Quartiles Decile
    // @param dataSet - Array of JSON objects - {"name":"Company Name","rating":60}
    prepare: function(dataSet) {

	dataSet = dataSet.sort(this.sortSet);

	// Divide into Qs
	var qindex = 1,
	qcount = 1;

	// Reset
	this.qdata = new Array(),
	this.data = new Array(),
	this.categories = new Array();

	for (i=0;i<dataSet.length;i++) {
            if (this.qdata[qindex]===null||!this.qdata[qindex]) this.qdata[qindex] = new Array();

	    if (this.inFirstDecileP(i,dataSet.length))
		dataSet[i] = {"name":dataSet[i].name, "cid": dataSet[i].CID, "y":dataSet[i].rating, "color":'#FF0000'};
	    else
		dataSet[i] = {"name":dataSet[i].name, "cid": dataSet[i].CID, "y":dataSet[i].rating};

            this.qdata[qindex].push(dataSet[i]);
	    if (Math.round(dataSet.length/4) == qcount) {
		qcount = 1;
		qindex++;
		continue;
            }
	    qcount++;
	}    

	// Prepare data and category properties
	for (var i=0; i<4; i++) {
	    this.categories[i+1] = this.getCategories(i+1);
	    this.data[i+1] = this.getData(i+1);
	}


    },

    inFirstDecileP: function(i,n) {
	var last = (n / 10);
	return (i < last);
    },

    getCategories: function(q,startIndex,endIndex) {
	    var categories = [],
            i=startIndex || this.qcurrentIndex,
            z=endIndex||this.qmaxItems;

        while(i<z){
            if(this.qdata[q].length>i){
                categories.push(this.qdata[q][i].name);
                i++;
            }
            else{
                i=z;
            }
        }

	return categories;
    },

    getData: function(q,startIndex,endIndex) {
	    var data = [];
        var i=startIndex || this.qcurrentIndex,
            z=endIndex||this.qmaxItems;

        while(i<z){
            if(this.qdata[q].length>i){
                data.push({y:this.qdata[q][i].y,color:this.qdata[q][i].color});
                i++;
            }
            else{
                i=z;
            }
        }
        return data;
    },

    showQuartile: function(q,startIndex,endIndex) {

	var chart = $('#container1').highcharts();

	for (var i=0; i<4; i++) {
	    if (i==q-1) {
		    chart.series[i].setVisible(true,false);
	    	$("#q"+(i+1)).css("background-color",chart.options.colors[i]);
            chart.series[i].setData(this.getData(q,startIndex,endIndex),false);
        } else {
		    chart.series[i].setVisible(false,false);
		    $("#q"+(i+1)).css("background-color","white");
	    }
	}

    chart.xAxis[0].setCategories(this.getCategories(q,startIndex,endIndex),false);

	this.qcurrent = q;

	chart.redraw();
	
    },

    sortSet: function(a,b) {return b.rating - a.rating}

};

