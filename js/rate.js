$(function(){

// Sort the entire set and divide into quartiles.
bwistats.prepare(dataSet);

var data = bwistats.data, 
    categories = bwistats.categories,
    maxItems=9;

var chart1 = new Highcharts.Chart({

    chart: {
        renderTo: 'container1',
        animation: false,
        zoomType: 'x'
    },
    title: {
	text: chart_title
    },
    tooltip: {
        valueDecimals: 1
    },
    legend: {
	enabled: false
    },
    xAxis: {
        categories: categories[1],
        max:maxItems
    },
    yAxis: {
	min: 0,
        max: 100,
	allowDecimals: false,
	title: {
	    text: 'Rating'
	}
    },

    plotOptions: {
        series: {
            point: {
                events: {
                    drag: function (e) {
                        // While dragging...
                    },
                    drop: function () {		

			            for (var data_index=0; data_index < this.series.data.length; data_index++)
			                if (this.series.data[data_index].category == this.category) break;

			            // Update quartile data, but do not sort
			            bwistats.qdata[this.series.index+1][data_index].y = this.y;

                        // Save rating to DB
                        var cid = bwistats.qdata[this.series.index+1][data_index].cid;
                        /*
                        $.ajax({
                            type: "POST",
                            url: "rating-save.php",
                            data: { IID: iid, CID: cid, Rating: this.y }
                        })
                        .fail(function() {
                            alert('Error saving rating!')
                        });*/
                        
                    }
                }
            },
            cursor: 'ew-resize',
            draggableY: true,
            dragMinY: 0,
            dragMaxY: 100,
            dragSnapInterval: 1,
	    minPointLength: 2
        }
    },
    series: 
    [{
	type: 'bar',
	name: 'Q1',
	data: data[1]
    },{
	type: 'bar',
	name: 'Q2',
	data: data[2]
    },{
	type: 'bar',
	name: 'Q3',
	data: data[3]
    },{
	type: 'bar',
	name: 'Q4',
	data: data[4]
    }],
    credits: {
	enabled: false
    }
});

    // Default to the first Q.
    bwistats.showQuartile(1,0);


Highcharts.Point.prototype.tooltipFormatter = function (useHeader) {
    var point = this,
        series = point.series;
    return ['<span style="color:' + series.color + '">', (point.name || series.name), '</span>: ', (!useHeader ? ('<b>x = ' + (point.name || point.x) + ',</b> ') : ''),
        //'<b>', (!useHeader ? 'x,y = ' : ''), Highcharts.numberFormat(point.x, 1),
        //', ', 
	Highcharts.numberFormat(point.y, 1),
        '</b>'].join('');
};

$('.qbuttons').click(function() {
    var chart = $('#container1').highcharts(),
    qindex = this.id.substr(1);

    //reset scroll
    bwistats.qcurrentIndex=0;
    bwistats.qlastIndex=bwistats.qmaxItems;
    // Show current Q
    bwistats.showQuartile(qindex);
});

$('#rerank').click(function() {
    var chart = $('#container1').highcharts(),
    dataSet = new Array();

    // Join modified Q data
    dataSet = bwistats.qdata[1].concat(bwistats.qdata[2],bwistats.qdata[3],bwistats.qdata[4]);
    
    // Transform dataSet {name,y} to {name,rating} as expected by prepare
    for (var i=0; i<dataSet.length; i++) {
        dataSet[i].rating = dataSet[i].y;
        delete dataSet[i].y;
    }
    
    // Sort and divide into quartiles
    bwistats.prepare(dataSet);

    // Set reranked series data and cats
    for (var q=1; q<=4; q++) {
	chart.series[q-1].setData(bwistats.data[q],false);
	chart.xAxis[0].setCategories(bwistats.categories[q],false);
    }

    // Show current Q
    bwistats.showQuartile(bwistats.qcurrent);
    
});
var timeout;

    function scrollUp(){
        bwistats.qcurrentIndex=Math.max(bwistats.qcurrentIndex-1,0);
        bwistats.qlastIndex=Math.max(bwistats.qlastIndex-1,bwistats.qmaxItems);
        bwistats.showQuartile(bwistats.qcurrent,bwistats.qcurrentIndex,bwistats.qlastIndex)
        if(bwistats.qcurrentIndex==0){
            $(this).prop('disabled', true);
        }
        $(".down").prop('disabled', false);
    }

    function scrollDown(){
        bwistats.qcurrentIndex++;
        bwistats.qlastIndex++;
        bwistats.showQuartile(bwistats.qcurrent,bwistats.qcurrentIndex,bwistats.qlastIndex)
        if(bwistats.qlastIndex >= bwistats.qdata[bwistats.qcurrent].length){
            $(this).prop('disabled', true);
        }
        $(".up").prop('disabled', false);
    }

    $(".up").click(scrollUp);
    $(".down").click(scrollDown);

$(".up").mousedown(function(){
    timeout = setInterval(scrollUp, 100);
    return false;
});

$(".down").mousedown(function(){
    timeout = setInterval(scrollDown, 100);
    return false;
});

    $(document).mouseup(function(){
        clearInterval(timeout);
    })

});
