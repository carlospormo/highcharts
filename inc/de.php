<?php

// Data Entry classs for BWI DE v2.0

class de {

	// Total number of companies
	const TOTAL_RATINGS_COUNT = 100;

    public static function get_ratings() {
        
        $ratings = array();

        $i=1;
        while ($i <= self::TOTAL_RATINGS_COUNT) {
            $ratings[] = array('name'   => chr(rand(65,90)).chr(rand(65,90)).chr(rand(65,90)),
							   'rating' => rand(1,100));
            $i++;
        }        
        
        $json = json_encode($ratings);

        return $json;    	
    }

}

?>