<?php

require_once "inc/de.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <title>Data Entry 2.0 - Interactive Options</title>
  
  <script type='text/javascript' src='js/jquery/jquery-1.7.1.js'></script>
  
  <link rel="stylesheet" type="text/css" href="css/style.css"/>

  <script>
    var dataSet = <?php echo de::get_ratings(); ?>;
    var chart_title = "<?php echo "Chart Title"; ?>";
  </script>
    
  <script src="js/rate.js"></script>

</head>
<body>
<script src="js/highcharts/highcharts.js"></script>
<script src="js/highcharts/highcharts-more.js"></script>
<script src="js/draggable-points.js"></script>
<script src="js/bwistats.js"></script>

<br>

<br>
<div id="content">
  <div class="q-container">
    <div id="q1" class="qbuttons">Q1</div>
    <div id="q2" class="qbuttons">Q2</div>
    <div id="q3" class="qbuttons">Q3</div>
    <div id="q4" class="qbuttons">Q4</div>
  </div>

  <div id="container1"></div>
    <div class="p-container">
        <button class="up">Up</button>
        <button class="down">Down</button>
    </div>
  <button id="rerank">Update Rank</button>
</div>

</body>
</html>
